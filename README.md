## Python Address Book Project

A simple project that presents the users options to create and view contacts (list)

* All data is stored in memory (dictionaries and lists)
* No fancy CSS, just barebones HTML based UI

Uses 'Flask' to serve/render the pages

* Please install the 'Flask' module for the application to work. See the link below for more details:
    * https://palletsprojects.com/p/flask/