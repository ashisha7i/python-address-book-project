from flask import Flask, render_template, request

server = Flask(__name__)

# Creating the global variable where the contacts will be stored
contacts_list = [] # Type List - this will store 'contacts' in the format

@server.route("/")
@server.route("/home")
def home_page():
    return render_template("home.html")


@server.route("/add")
def add_contact():
    return render_template("add.html")

@server.route("/list")
def list_contacts():
    return render_template("list.html", **globals())

@server.route("/about")
def show_about():
    return render_template("about.html")

@server.route("/confirm", methods=["POST"])
def show_confirmation():
    contact = {} # Initializing an empty dictionary for this contact
    contact['first_name'] = request.form.get('first_name')
    contact['last_name'] = request.form.get('last_name')
    contact['email'] = request.form.get('email')
    contact['phone'] = request.form.get('phone')

    # Save the contact in the main list
    contacts_list.append(contact);

    return render_template("confirmation.html")

# ---- Running the server ----
if __name__ == '__main__':
    server.run(debug=True)